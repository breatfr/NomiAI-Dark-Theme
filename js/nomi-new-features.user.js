// ==UserScript==
// @name        Nomi - New features
// @description New features for Nomi
// @match       https://*.nomi.ai/*
// @version     1.0.0
// @author      BreatFR
// @namespace   https://gitlab.com/breatfr
// @downloadURL https://gitlab.com/breatfr/nomi/-/raw/main/js/nomi-new-features.user.js
// @updateURL   https://gitlab.com/breatfr/nomi/-/raw/main/js/nomi-new-features.user.js
// @icon        https://assets.nomi.ai/_next/static/media/favicon.49a32755.svg
// @license        AGPL-3.0-or-later; https://www.gnu.org/licenses/agpl-3.0.txt
// ==/UserScript==

(function() {
    'use strict';

// Textarea auto focus
    function focusTextarea() {
        var textareaElement = document.querySelector('.css-d56yar');
        if (textareaElement) {
            textareaElement.focus();
        }
    }

    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            focusTextarea();
        });
    });

    observer.observe(document.body, { childList: true, subtree: true });

    focusTextarea();
})();
